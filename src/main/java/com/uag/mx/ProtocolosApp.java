package com.uag.mx;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;

import com.uag.mx.controllers.COMController;
import com.uag.mx.controllers.MQTTListenerCallback;
import com.uag.mx.entities.MQTTClient;


public class ProtocolosApp {
	static Logger logger = Logger.getLogger(ProtocolosApp.class.getName());
	
    public static void main( String[] args ){
    	BasicConfigurator.configure();
    	if(args.length < 1) {
    		logger.error("The config file can not be empty");
    		System.exit(1);
    	} else {
    		String file = args[0];
    		
        	String portName = "/dev/ttyACM0";
        	logger.info("Using the followig config file ====> " + file);
        	logger.info("Opening the device port ===> " + portName);
        	COMController com = new COMController();
        	
        	logger.info("Starting to generate the mqtt connection");
        	
        	com.uag.mx.entities.MQTTClient simpleClient = new MQTTClient("java", "tcp://52.14.81.188:5000", MqttClient.generateClientId(), 2);
        	com.uag.mx.controllers.MQTTClient mqttClient = new com.uag.mx.controllers.MQTTClient(simpleClient);	
        	
        	try {
        		logger.info("Connecting to ===> " + portName);
        		com.connect(portName);
    			mqttClient.connect();
    			MqttClient subcriber = mqttClient.getMqttClient();
    			subcriber.setCallback(new MQTTListenerCallback(com));
    			subcriber.subscribe("java");
    		} catch (MqttException e) {
    			logger.error("Error when the mqtt connection was creating");
    		} catch (Exception e) {
    			logger.error("Error trying open the port ===> " + portName);
    		}
    	}
    	
    }
}
