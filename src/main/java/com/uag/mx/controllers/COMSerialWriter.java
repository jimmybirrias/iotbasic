package com.uag.mx.controllers;

import java.io.OutputStream;

import org.apache.log4j.Logger;

public class COMSerialWriter implements Runnable {
	static Logger logger = Logger.getLogger(COMSerialWriter.class.getName());
	private StringBuffer buffer = new StringBuffer();

	private OutputStream out;
	
	public COMSerialWriter(OutputStream out) {
		this.out = out;
	}
	
	public void run() {
		try {
			while (true) {
				if (buffer.toString().equals("")) {
					//logger.warn("No mqtt message");
				} else {
					logger.info("Writing value the following message ===> " + buffer.toString());
					out.write(buffer.toString().getBytes());
					logger.warn("Cleaning buffer");
					buffer.delete(0, buffer.length());
				}
				Thread.sleep(1000);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void write(String newMessage) {
		buffer.append(newMessage);
	}
	
}
