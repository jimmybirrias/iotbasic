package com.uag.mx.controllers;

import java.io.InputStream;
import java.io.OutputStream;

import org.apache.log4j.Logger;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;

public class COMController {
	static Logger logger = Logger.getLogger(COMController.class.getName());
	private InputStream in;
	private OutputStream out;

	public void connect(String portName) throws Exception {
		CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(portName);
		
		if (portIdentifier.isCurrentlyOwned()) {
			logger.error("Error, [ " + portName + " ] is currently in use");
		} else {
			int timeout = 2000;
			CommPort commPort = portIdentifier.open(this.getClass().getName(), timeout);
			
			if(commPort instanceof SerialPort) {
				SerialPort serialPort = (SerialPort) commPort;
				serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1,
						SerialPort.PARITY_NONE);
				
				in = serialPort.getInputStream();
				out = serialPort.getOutputStream();
				
				
			} else {
				logger.error("Error, [ " + portName+" ], is not serial port type");
			}
		}
		
	}

	public InputStream getIn() {
		return in;
	}

	public OutputStream getOut() {
		return out;
	}	
	
}
