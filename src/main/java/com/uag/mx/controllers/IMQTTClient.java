package com.uag.mx.controllers;

public interface IMQTTClient {
	void sendMessage(String message);
}
