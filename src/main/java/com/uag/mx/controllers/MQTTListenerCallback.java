package com.uag.mx.controllers;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class MQTTListenerCallback implements MqttCallback{
	static Logger logger = Logger.getLogger(MQTTListenerCallback.class.getName());
	COMController comController;
	COMSerialWriter cwriter;
	
	public MQTTListenerCallback(COMController comController) {
		this.comController = comController;
		cwriter = new COMSerialWriter(comController.getOut());
		new Thread(cwriter).start();
	}
	
	public void connectionLost(Throwable cause) {
		logger.error("Connectiont to MQTT broker lost");
	}

	public void messageArrived(String topic, MqttMessage message) throws Exception {
		logger.info("MQTT message received ===> [ topic: " + topic + "] Command: " + new String(message.getPayload()));
		cwriter.write(new String(message.getPayload()));
	}

	public void deliveryComplete(IMqttDeliveryToken token) {
		
	}
}
