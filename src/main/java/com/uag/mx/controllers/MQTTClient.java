package com.uag.mx.controllers;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class MQTTClient implements IMQTTClient{
	static Logger logger = Logger.getLogger(MQTTClient.class.getName());
	MqttClient mqttClient;
	MqttConnectOptions mqttOptions;
	com.uag.mx.entities.MQTTClient simpleClient;
	MemoryPersistence persistence;
	
	
	public MQTTClient(com.uag.mx.entities.MQTTClient client) {
		logger.info("Starting the connection to broker using the following client ===> " + client.toString());
		this.simpleClient = client;
		persistence = new MemoryPersistence();
		mqttOptions = new MqttConnectOptions();
		mqttOptions.setCleanSession(true);
	}
	
	public void connect() throws MqttException {
		logger.info("Generating the mqttclient");
		mqttClient = new MqttClient(simpleClient.getBroker(), simpleClient.getClientId(), persistence);
		mqttClient.connect(mqttOptions);
		logger.info("Connect to the boker ===> " + simpleClient.getBroker());
	}
	
	public void disconnect() {
		try {
			mqttClient.disconnect();
		} catch (MqttException e) {
			logger.error("Failed to disconnect the client, error ===> " + e.getMessage());
		}
	}

	public void sendMessage(String message) {
		logger.info("The following message will be published ===> " + message);
		MqttMessage msg = new MqttMessage(message.getBytes());
		msg.setQos(simpleClient.getQos());
		try {
			mqttClient.publish(simpleClient.getTopic(), msg);
		} catch (MqttPersistenceException e) {
			logger.error("Failed to publish the message, error ===> " + e.getMessage());
		} catch (MqttException e) {
			logger.error("Failed to publish the message, error ===> " + e.getMessage());
		}
	}

	public MqttClient getMqttClient() {
		return mqttClient;
	}
}
