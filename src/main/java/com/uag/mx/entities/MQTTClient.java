package com.uag.mx.entities;

public class MQTTClient {
	private String topic;
	private String broker;
	private String clientId;
	private int qos;
	
	public MQTTClient(String topic, String broker, String clientId, int qos) {
		super();
		this.topic = topic;
		this.broker = broker;
		this.clientId = clientId;
		this.qos = qos;
	}
	
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public String getBroker() {
		return broker;
	}
	public void setBroker(String broker) {
		this.broker = broker;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	
	public int getQos() {
		return qos;
	}

	public void setQos(int qos) {
		this.qos = qos;
	}

	@Override
	public String toString() {
		return "MQTTClient [topic=" + topic + ", broker=" + broker + ", clientId=" + clientId + "]";
	}
}
